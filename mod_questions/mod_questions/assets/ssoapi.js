
var ssoapi =
{
    api_url: 'http://testauth.hsmedia.ru',
    oauth1_url: '/api/oauth1_login',
    oauth2_url: '/api/oauth2_login',
    site_id: '',
    oauth_window: null,
    timeoutID: 0,

    cookie_setter: function(callback)
    {
        document.write("<script src=\""+this.api_url+"/api/cookie?callback_uri="+encodeURI(callback)+"\"></script>");
    },
    popup_checker: function(callback)
    {
        if (this.oauth_window && this.oauth_window.closed)
        {
            window.clearInterval(this.timeoutID);
            window.location=callback;
        }
        var self=this;
        this.timeoutID=window.setTimeout(
            function(){
                self.popup_checker(callback);
            }
            , 500);

    },
    oauth_auto_popup: function(elem)
    {
        this.oauth_window=window.open(elem.href,'_blank','width=800,height=400,status=no,titlebar=no,scrollbars=no,toolbar=no,resizable=no,menubar=no,location=no');
        var self=this;
        var pattern=new RegExp('fail_callback=([^&]*)');

        var callback=pattern.exec(elem.href)[1];
        callback=decodeURI(callback);
        //decodeURI doesn't work =/
        callback=callback.replace(/%3A/g,':');
        callback=callback.replace(/%2F/g,'/');
        this.timeoutID=window.setTimeout(
            function(){
                self.popup_checker(callback);
            }
            , 500);
        return false;
    },
    oauth_popup: function(type,success_callback,fail_callback)
    {
        this.oauth_window=window.open(this.get_oauth_link(type, success_callback, fail_callback),'_blank','width=800,height=400,status=no,titlebar=no,scrollbars=no,toolbar=no,resizable=no,menubar=no,location=no');
        var self=this;
        var callback=fail_callback;
        this.timeoutID=window.setTimeout(
            function(){
                self.popup_checker(callback);
            }
            , 500);
        return false;
    },
    get_oauth_link: function(type,success_callback,fail_callback)
    {
        switch (type)
        {
            case "twitter":
                return this.api_url+this.oauth1_url+'/'+type+'?redirect_url='+encodeURI(success_callback)+'&fail_callback='+encodeURI(fail_callback)+'&site_id='+this.site_id;
                break;
            default:
                return this.api_url+this.oauth2_url+'/'+type+'?redirect_url='+encodeURI(success_callback)+'&fail_callback='+encodeURI(fail_callback)+'&site_id='+this.site_id;
                break;
        }
    }
}
