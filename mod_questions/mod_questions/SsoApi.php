<?php
/**
* Client-side implementation of sso-server api
*
* You'll need curl enabled for this to work
*
* Documentation can be found in doc/ folder.
*
* @author Epifanov Ivan <iepifanov@hsmedia.ru>
* @copyright Hearst-Shkulev Media, 2012
* @package sso
*/



/**
* Class for dealing with sso
* @package sso
*/
class SsoApi
{
    const SSO_ERROR_GENERIC = 500;

    const SSO_URL_COOKIE = '/api/cookie';
    const SSO_URL_LOGIN = '/api/login';
    const SSO_URL_SECURE_LOGIN = '/api/secure_login';
    const SSO_URL_FORCE_LOGIN = '/api/force_login';
    const SSO_URL_LOGIN2MAIL = '/api/login2mail';
    const SSO_URL_NICK2MAIL = '/api/nickname2mail';
    const SSO_URL_LOGOUT = '/api/logout';
    const SSO_URL_REGISTER = '/api/register';
    const SSO_URL_EDIT = '/api/edit';
    const SSO_URL_AUTH_COOKIE = '/api/set_auth_cookie';
    const SSO_URL_DATA = '/api/data';
    const SSO_URL_REVISION = '/api/revision';
    const SSO_URL_OAUTH1 = '/api/oauth1_login';
    const SSO_URL_OAUTH2 = '/api/oauth2_login';
    const SSO_URL_TOKEN = '/api/by_token';
    const SSO_URL_CHECK = '/api/check';
    const SSO_URL_DETACH = '/api/detach';
    const SSO_URL_FINDBYLOGIN = '/api/findbylogin';
    const SSO_URL_LASTLOGIN = '/api/updatelastlogin';



    protected $_app_id;
    protected $_app_key;
    protected $_sso_url;
    protected $_error;
    private $_auth_url;
    private $_token;

    /**
     * Constructor
     * @param string $app_id application id
     * @param string $app_key application key
     * @param string $sso_url SSO server base url
     */
    public function __construct($app_id, $app_key, $sso_url)
    {
        $this->_app_id = $app_id;
        $this->_app_key = $app_key;
        $this->_sso_url = $sso_url;
    }

    /**
     * return code of last error
     * @return integer error code
     */
    public function getError()
    {
        return $this->_error;
    }

    /**
     * get url for cookie-setter
     * @param string $callback url to call after setting cookie
     * @return string url
     */
    public function cookieUrl($callback = NULL)
    {
        return $this->_sso_url.SsoApi::SSO_URL_COOKIE.(isset($callback) ? "?callback_url=".urlencode($callback) : '');
    }

    /**
     * get url for oauth auth
     * @param string $type oauth type
     * @param string $success_callback url to call on success
     * @param string $fail_callback url to call on user cancel
     * @return string url
     */
    public function oauthUrl($type,$success_callback,$fail_callback)
    {
        switch ($type)
        {
            case 'twitter':
                return $this->_sso_url.SsoApi::SSO_URL_OAUTH1.'/'.$type.'?redirect_url='.urlencode($success_callback).'&fail_callback='.urlencode($fail_callback).'&site_id='.$this->_app_id;
                break;
            default:
                return $this->_sso_url.SsoApi::SSO_URL_OAUTH2.'/'.$type.'?redirect_url='.urlencode($success_callback).'&fail_callback='.urlencode($fail_callback).'&site_id='.$this->_app_id;
                break;
        }
    }

    /**
     * Login user to sso
     * @param string $email user email
     * @param string $password user password (plaintext)
     * @param int $remember 1 - keep user logged in, 0 - keep logged in until user closes browser
     * @return bool true on success
     */
    public function login($email, $password,$remember=1)
    {
        $data = array(
            'data' => json_encode(
                    array(
                        "email" => $email,
                        "password" => $password,
                        "remember" => $remember
                    )
            ),
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_LOGIN, $data);

        if (isset($result['auth_url']))
        {
            $this->_auth_url = $result['auth_url'];
            $this->_token = $result['token'];
            return TRUE;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }


    /**
     * Login user to sso
     * @param string $email user email
     * @param string $password user password (plaintext)
     * @param int $remember 1 - keep user logged in, 0 - keep logged in until user closes browser
     * @return bool true on success
     */
    public function secureLogin($email, $password,$remember=1)
    {
        $data = array(
            'data' => json_encode(
                    array(
                        "email" => $email,
                        "password" => sha1($password),
                        "remember" => $remember
                    )
            ),
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_SECURE_LOGIN, $data);

        if (isset($result['auth_url']))
        {
            $this->_auth_url = $result['auth_url'];
            $this->_token = $result['token'];
            return TRUE;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * Login user to sso without password check
     * @param string $email user email
     * @param int $remember 1 - keep user logged in, 0 - keep logged in until user closes browser
     * @return boolean true on success
     */
    public function forceLogin($email,$remember=1)
    {
        $data = array(
            'data' => json_encode(
                    array(
                        "email" => $email,
                        "remember" => $remember
                    )
            ),
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_FORCE_LOGIN, $data);

        if (isset($result['auth_url']))
        {
            $this->_auth_url = $result['auth_url'];
            $this->_token = $result['token'];
            return TRUE;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * Return full url to logout endpoint
     * @param string $callback callbect url to redirect after login
     * @return string url to redirect to
     */
    public function logoutUrl($callback = NULL)
    {
        return $this->_sso_url.SsoApi::SSO_URL_LOGOUT.(isset($callback) ? "?callback_url=".urlencode($callback) : '');
    }

    /**
     * returns url for session sign-on to sso
     * @return string url to redirect to
     */
    public function getAuthUrl()
    {
        return $this->_auth_url.'?token='.$this->_token;
    }

    /**
     * Create new user
     * @param Array $data user data
     * @return boolean true on success
     */
    public function register($data)
    {
        $data = array(
            'data' => json_encode(
                    $data
            ),
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_REGISTER, $data);

        if (isset($result['success']))
        {
            return TRUE;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * Update user info
     * @param string $email user email
     * @param Array $data new data to set
     * @return boolean true on success
     */
    public function edit($email, $data)
    {
        $data = array(
            'data' => json_encode(
                    $data
            ),
            "email" => $email,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_EDIT, $data);

        if (isset($result['success']))
        {
            return TRUE;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * ban user (shorthand for edit($email,array('banned'=>true)) )
     * @param string $email user email
     * @return boolean true on success
     */
    public function ban($email)
    {
        $data = array(
            'data' => json_encode(
                    array('banned'=>true)
            ),
            "email" => $email,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_EDIT, $data);

        if (isset($result['success']))
        {
            return TRUE;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * unban user (shorthand for edit($email,array('banned'=>false)) )
     * @param string $email user email
     * @return boolean true on success
     */
    public function unban($email)
    {
        $data = array(
            'data' => json_encode(
                    array('banned'=>false)
            ),
            "email" => $email,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_EDIT, $data);

        if (isset($result['success']))
        {
            return TRUE;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * set user as having valid email
     * @param string $email user email
     * @return boolean true on success
     */
    public function setValid($email)
    {
        $data = array(
            'data' => json_encode(
                    array(
                        'verified' => true,
                        'mail_verified' => true,
                        'verification' => md5(uniqid(rand(), true))
                        )
            ),
            "email" => $email,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_EDIT, $data);

        if (isset($result['success']))
        {
            return TRUE;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * Return full user data
     * @param string $email user email
     * @return Array full dump of user record
     */
    public function getData($email)
    {
        $data = array(
            "email" => $email,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_DATA, $data);

        if (isset($result['email']))
        {
            return $result;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * Return full user data
     * @param string $token user verification token
     * @return Array full dump of user record
     */
    public function getByToken($token)
    {
        $data = array(
            "token" => $token,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_TOKEN, $data);

        if (isset($result['email']))
        {
            return $result;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * Return email for site-login
     * @param string $login user login (for current site)
     * @return string user email (global id)
     */
    public function getEmailByLogin($login)
    {
        $data = array(
            "login" => $login,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_LOGIN2MAIL, $data);

        if (isset($result['email']))
        {
            return $result['email'];
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }


    /**
     * Return email for site-login
     * @param string $login user login (for current site)
     * @return string user email (global id)
     */
    public function getEmailByNickname($nick)
    {
        $data = array(
            "nickname" => $nick,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_NICK2MAIL, $data);

        if (isset($result['email']))
        {
            return $result['email'];
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * Get current user version
     * @param string $email user email
     * @return string current user revision as set by couchdb
     */
    public function getRevision($email)
    {
        $data = array(
            "email" => $email,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        // grab URL
        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_REVISION, $data);

        if (isset($result['user']))
        {
            return $result;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * check, if user have active sso session
     * @param string $email user email
     * @param string $ip user ip
     * @param string $sess user sso session id
     * @return bool
     *
     */

    public function check($email,$ip, $sess)
    {
        $data = array(
            "email" => $email,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key,
            "ip" => $ip,
            "ssosession" => $sess
        );

        // grab URL
        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_CHECK, $data);

        if (isset($result) && isset($result['result']))
        {
            return $result['result'];
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * delete data for coresponding social net
     * @param string $email user email
     * @param string $type social net
     * @return bool 
     */
    public function detach($email,$type)
    {
        $data = array(
            "email" => $email,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key,
            "type" => $type
        );

        // grab URL
        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_DETACH, $data);

        if (isset($result) && isset($result['result']))
        {
            return $result['result'];
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * find sites for login/password pair
     * @param string $login user login
     * @param string $password user password
     * @return array
     */
    public function sitesByLogin($login,$password)
    {
        $data = array(
            "login" => $login,
            "password" => $password,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        // grab URL
        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_FINDBYLOGIN, $data);
        if (isset($result) && isset($result['result']))
        {
            return $result['result'];
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * Update user last login date
     * @param string $email user email
     * @return boolean true on success
     */
    public function updateLastLogin($email)
    {
        $data = array(
            "email" => $email,
            "app_id" => $this->_app_id,
            "app_secret" => $this->_app_key
        );

        $result = $this->_request($this->_sso_url.SsoApi::SSO_URL_LASTLOGIN, $data);

        if (isset($result['success']))
        {
            return TRUE;
        }
        else
        {
            $this->_error = isset($result['error']) ? $result['error'] : SsoApi::SSO_ERROR_GENERIC;
            return FALSE;
        }
    }

    /**
     * Do a request to sso (internal)
     * @param string $url url co grab
     * @param Array $data post data
     * @return Array response data
     */
    protected function _request($url,$data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        //all data transfered through post-request
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        // grab URL
        $result = curl_exec($ch);
        if ($result)
            $result = json_decode($result, true);
        return $result;
    }

}
?>

