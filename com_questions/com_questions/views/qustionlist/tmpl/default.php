<?php
/**
 * @version     1.0.0
 * @package     com_questions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      al <ubrikon@gmail.com> - http://
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questions', JPATH_ADMINISTRATOR);
$canEdit = JFactory::getUser()->authorise('core.edit', 'com_questions');
if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_questions')) {
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>
<?php if ($this->item) : ?>

    <div class="item_fields">

        <ul class="fields_list">

            			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ID'); ?>:
			<?php echo $this->item->id; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ORDERING'); ?>:
			<?php echo $this->item->ordering; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_STATE'); ?>:
			<?php echo $this->item->state; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_CHECKED_OUT'); ?>:
			<?php echo $this->item->checked_out; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_CHECKED_OUT_TIME'); ?>:
			<?php echo $this->item->checked_out_time; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_CREATED_BY'); ?>:
			<?php echo $this->item->created_by; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_Q_TEXT'); ?>:
			<?php echo $this->item->q_text; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ANSWER_1'); ?>:
			<?php echo $this->item->answer_1; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ANSWER_1_WEIGHT'); ?>:
			<?php echo $this->item->answer_1_weight; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ANSWER_2'); ?>:
			<?php echo $this->item->answer_2; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ANSWER_2_WEIGHT'); ?>:
			<?php echo $this->item->answer_2_weight; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ANSWER_3'); ?>:
			<?php echo $this->item->answer_3; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ANSWER_3_WEIGHT'); ?>:
			<?php echo $this->item->answer_3_weight; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ANSWER_4'); ?>:
			<?php echo $this->item->answer_4; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ANSWER_4_WEIGHT'); ?>:
			<?php echo $this->item->answer_4_weight; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ANSWER_5'); ?>:
			<?php echo $this->item->answer_5; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_ANSWER_5_WEIGHT'); ?>:
			<?php echo $this->item->answer_5_weight; ?></li>
			<li><?php echo JText::_('COM_QUESTIONS_FORM_LBL_QUSTIONLIST_RIGHT_ANSWER'); ?>:
			<?php echo $this->item->right_answer; ?></li>


        </ul>

    </div>
    <?php if($canEdit): ?>
		<a href="<?php echo JRoute::_('index.php?option=com_questions&task=qustionlist.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_QUESTIONS_EDIT_ITEM"); ?></a>
	<?php endif; ?>
								<?php if(JFactory::getUser()->authorise('core.delete','com_questions')):
								?>
									<a href="javascript:document.getElementById('form-qustionlist-delete-<?php echo $this->item->id ?>').submit()"><?php echo JText::_("COM_QUESTIONS_DELETE_ITEM"); ?></a>
									<form id="form-qustionlist-delete-<?php echo $this->item->id; ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_questions&task=qustionlist.remove'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
										<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
										<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
										<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
										<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
										<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
										<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />
										<input type="hidden" name="jform[q_text]" value="<?php echo $this->item->q_text; ?>" />
										<input type="hidden" name="jform[answer_1]" value="<?php echo $this->item->answer_1; ?>" />
										<input type="hidden" name="jform[answer_1_weight]" value="<?php echo $this->item->answer_1_weight; ?>" />
										<input type="hidden" name="jform[answer_2]" value="<?php echo $this->item->answer_2; ?>" />
										<input type="hidden" name="jform[answer_2_weight]" value="<?php echo $this->item->answer_2_weight; ?>" />
										<input type="hidden" name="jform[answer_3]" value="<?php echo $this->item->answer_3; ?>" />
										<input type="hidden" name="jform[answer_3_weight]" value="<?php echo $this->item->answer_3_weight; ?>" />
										<input type="hidden" name="jform[answer_4]" value="<?php echo $this->item->answer_4; ?>" />
										<input type="hidden" name="jform[answer_4_weight]" value="<?php echo $this->item->answer_4_weight; ?>" />
										<input type="hidden" name="jform[answer_5]" value="<?php echo $this->item->answer_5; ?>" />
										<input type="hidden" name="jform[answer_5_weight]" value="<?php echo $this->item->answer_5_weight; ?>" />
										<input type="hidden" name="jform[right_answer]" value="<?php echo $this->item->right_answer; ?>" />
										<input type="hidden" name="option" value="com_questions" />
										<input type="hidden" name="task" value="qustionlist.remove" />
										<?php echo JHtml::_('form.token'); ?>
									</form>
								<?php
								endif;
							?>
<?php
else:
    echo JText::_('COM_QUESTIONS_ITEM_NOT_LOADED');
endif;
?>
