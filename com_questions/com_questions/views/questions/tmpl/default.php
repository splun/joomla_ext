<?php
/**
 * @version     1.0.0
 * @package     com_questions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      al <ubrikon@gmail.com> - http://
 */
// no direct access
defined('_JEXEC') or die;
?>

<div class="items">
    <ul class="items_list">
<?php $show = false; ?>
        <?php foreach ($this->items as $item) : ?>

            
				<?php
					if($item->state == 1 || ($item->state == 0 && JFactory::getUser()->authorise('core.edit.own',' com_questions'))):
						$show = true;
						?>
							<li>
								<a href="<?php echo JRoute::_('index.php?option=com_questions&view=qustionlist&id=' . (int)$item->id); ?>"><?php echo $item->q_text; ?></a>
								<?php
									if(JFactory::getUser()->authorise('core.edit.state','com_questions')):
									?>
										<a href="javascript:document.getElementById('form-qustionlist-state-<?php echo $item->id; ?>').submit()"><?php if($item->state == 1): echo JText::_("COM_QUESTIONS_UNPUBLISH_ITEM"); else: echo JText::_("COM_QUESTIONS_PUBLISH_ITEM"); endif; ?></a>
										<form id="form-qustionlist-state-<?php echo $item->id ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_questions&task=qustionlist.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
											<input type="hidden" name="jform[id]" value="<?php echo $item->id; ?>" />
											<input type="hidden" name="jform[ordering]" value="<?php echo $item->ordering; ?>" />
											<input type="hidden" name="jform[state]" value="<?php echo (int)!((int)$item->state); ?>" />
											<input type="hidden" name="jform[checked_out]" value="<?php echo $item->checked_out; ?>" />
											<input type="hidden" name="jform[checked_out_time]" value="<?php echo $item->checked_out_time; ?>" />
											<input type="hidden" name="jform[q_text]" value="<?php echo $item->q_text; ?>" />
											<input type="hidden" name="jform[answer_1]" value="<?php echo $item->answer_1; ?>" />
											<input type="hidden" name="jform[answer_1_weight]" value="<?php echo $item->answer_1_weight; ?>" />
											<input type="hidden" name="jform[answer_2]" value="<?php echo $item->answer_2; ?>" />
											<input type="hidden" name="jform[answer_2_weight]" value="<?php echo $item->answer_2_weight; ?>" />
											<input type="hidden" name="jform[answer_3]" value="<?php echo $item->answer_3; ?>" />
											<input type="hidden" name="jform[answer_3_weight]" value="<?php echo $item->answer_3_weight; ?>" />
											<input type="hidden" name="jform[answer_4]" value="<?php echo $item->answer_4; ?>" />
											<input type="hidden" name="jform[answer_4_weight]" value="<?php echo $item->answer_4_weight; ?>" />
											<input type="hidden" name="jform[answer_5]" value="<?php echo $item->answer_5; ?>" />
											<input type="hidden" name="jform[answer_5_weight]" value="<?php echo $item->answer_5_weight; ?>" />
											<input type="hidden" name="jform[right_answer]" value="<?php echo $item->right_answer; ?>" />
											<input type="hidden" name="option" value="com_questions" />
											<input type="hidden" name="task" value="qustionlist.save" />
											<?php echo JHtml::_('form.token'); ?>
										</form>
									<?php
									endif;
									if(JFactory::getUser()->authorise('core.delete','com_questions')):
									?>
										<a href="javascript:document.getElementById('form-qustionlist-delete-<?php echo $item->id; ?>').submit()"><?php echo JText::_("COM_QUESTIONS_DELETE_ITEM"); ?></a>
										<form id="form-qustionlist-delete-<?php echo $item->id; ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_questions&task=qustionlist.remove'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
											<input type="hidden" name="jform[id]" value="<?php echo $item->id; ?>" />
											<input type="hidden" name="jform[ordering]" value="<?php echo $item->ordering; ?>" />
											<input type="hidden" name="jform[state]" value="<?php echo $item->state; ?>" />
											<input type="hidden" name="jform[checked_out]" value="<?php echo $item->checked_out; ?>" />
											<input type="hidden" name="jform[checked_out_time]" value="<?php echo $item->checked_out_time; ?>" />
											<input type="hidden" name="jform[created_by]" value="<?php echo $item->created_by; ?>" />
											<input type="hidden" name="jform[q_text]" value="<?php echo $item->q_text; ?>" />
											<input type="hidden" name="jform[answer_1]" value="<?php echo $item->answer_1; ?>" />
											<input type="hidden" name="jform[answer_1_weight]" value="<?php echo $item->answer_1_weight; ?>" />
											<input type="hidden" name="jform[answer_2]" value="<?php echo $item->answer_2; ?>" />
											<input type="hidden" name="jform[answer_2_weight]" value="<?php echo $item->answer_2_weight; ?>" />
											<input type="hidden" name="jform[answer_3]" value="<?php echo $item->answer_3; ?>" />
											<input type="hidden" name="jform[answer_3_weight]" value="<?php echo $item->answer_3_weight; ?>" />
											<input type="hidden" name="jform[answer_4]" value="<?php echo $item->answer_4; ?>" />
											<input type="hidden" name="jform[answer_4_weight]" value="<?php echo $item->answer_4_weight; ?>" />
											<input type="hidden" name="jform[answer_5]" value="<?php echo $item->answer_5; ?>" />
											<input type="hidden" name="jform[answer_5_weight]" value="<?php echo $item->answer_5_weight; ?>" />
											<input type="hidden" name="jform[right_answer]" value="<?php echo $item->right_answer; ?>" />
											<input type="hidden" name="option" value="com_questions" />
											<input type="hidden" name="task" value="qustionlist.remove" />
											<?php echo JHtml::_('form.token'); ?>
										</form>
									<?php
									endif;
								?>
							</li>
						<?php endif; ?>

<?php endforeach; ?>
        <?php
        if (!$show):
            echo JText::_('COM_QUESTIONS_NO_ITEMS');
        endif;
        ?>
    </ul>
</div>
<?php if ($show): ?>
    <div class="pagination">
        <p class="counter">
            <?php echo $this->pagination->getPagesCounter(); ?>
        </p>
        <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
<?php endif; ?>


									<?php if(JFactory::getUser()->authorise('core.create','com_questions')): ?><a href="<?php echo JRoute::_('index.php?option=com_questions&task=qustionlist.edit&id=0'); ?>"><?php echo JText::_("COM_QUESTIONS_ADD_ITEM"); ?></a>
	<?php endif; ?>